/**
 * Created by Mohammmed Abdelbarry on 25-Aug-16.
 */
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var fs = require("fs-extra");
var session = require("express-session");
var passwordHash = require("password-hash");
var gm = require("gm");
const port = 3003;
const hashHelper1 = "i/4k0u7&4b05413d&b4rr/";
const hashHelper2 = "54n4+74n/4+m/+455";
// Create application/x-www-form-urlencoded parser
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json({limit: '10mb'}));
app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', "http://localhost:" + port);

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});
app.post("/register", function(req, res) {
    console.log("BEGIN: User Registration");
    const name = req.body.name;
    const email = req.body.email;
    var password = req.body.password;
    console.log("Registration request body: " + JSON.stringify(req.body));
    const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (name == null || email == null || password == null) {
        res.status(400).send("please fill all the fields in the registration form");
        console.log("Error: please fill all the fields in the registration form");
        return;
    }
    password = password.trim();
    if (password.length < 6) {
        res.status(400).send("The password is too short");
        console.log("Error: The password is too short");
        return;
    }
    if (!emailRegex.test(email)) {
        res.status(400).send(JSON.stringify("Incorrect email format"));
        console.log("Error: Incorrect email format");
        return;
    }
    const nameRegex = /^[a-zA-Z ]{1,31}$/;
    if (!nameRegex.test(name)) {
        res.status(400).send(JSON.stringify("Username contains unsupported characters"));
        console.log("Error: Username contains unsupported characters");
        return;
    }
    var usersData;
    var currentID = 0;
    //TODO properties
    fs.readFile(__dirname + "/database/" + "users.json", "utf-8", function(err, data) {
        if(err) {
            console.log("failed to open users.json: %s", err);
        }
        try {
            usersData = JSON.parse(data);
        } catch(ex) {
            console.log("failed to parse users.json: %s", ex);
        }
        for(var user in usersData) {
            currentID++;
            if (usersData[user].email === email.toLowerCase()) {
                res.status(409).send(JSON.stringify("Email already registered"));
                console.log("Error: Email already registered");
                return;
            }
        }
        if (currentID == 0) {
            usersData = {};
        }
        usersData["user" + currentID] = {
            email: email.toLowerCase(),
            password: passwordHash.generate(password.toString()),
            name: name,
            id: currentID
        };
        fs.mkdirs(__dirname + "/database/data/", function(err) {
            fs.writeFile(__dirname + "/database/" + "users.json",JSON.stringify(usersData), function (err) {
                if (err) {
                    console.log("failed to write new user into the database: %s", err);
                    res.status(500).send("failed to write user's data");
                    return;
                }
            });
            var user = {
                name: name,
                id: currentID,
                token: passwordHash.generate(hashHelper1 + currentID.toString() + hashHelper2),
                profilePicture: null,
                listViewMode: false,
                foldersCount: 0,
                folders: []
            };
            fs.writeFile(__dirname + "/database/data/" + "user" + currentID + ".json", JSON.stringify(user), function (err) {
                if (err) {
                    console.log("failed to write new user's data: %s", err);
                    res.status(500).send("failed to write new user's data");
                    return;
                }
            });
            res.status(201).send(JSON.stringify(user));
            console.log("END: User Registration");
        });
    });
});
app.post('/login', function(req, res) {
    console.log("BEGIN: User Login");
    const email = req.body.email;
    const password = req.body.password;
    var loggedInUser = null;
    var id = 0;
    console.log("Login request body: " + JSON.stringify(req.body));
    fs.readFile(__dirname + "/database/" + "users.json", "utf-8", function(err, data) {
        var usersData = null;
        if(err) {
            console.log("failed to open users.json: %s", err);
        }
        try {
            usersData = JSON.parse(data);
        } catch(ex) {
            console.log("failed to parse users.json: %s", ex);
        }
        if (usersData == null) {
            res.status(400).send("Incorrect email or password");
            console.log("Error: Incorrect email or password");
            return;
        }
        for(var user in usersData) {
            if (usersData[user].email === email.toLowerCase()
                && passwordHash.verify(password.toString(), usersData[user].password)) {
                loggedInUser = usersData[user];
                id = usersData[user].id;
            }
        }
        if (loggedInUser == null) {
            res.status(400).send("Incorrect email or password");
            console.log("Error: Incorrect email or password");
        } else {
            fs.readFile(__dirname + "/database/data/" + "user" + loggedInUser.id + ".json", 'utf-8', function(err, data) {
                if (err) {
                    console.log("failed to open users" + id + ".json" ,err);
                    res.status(500).send("Failed to retrieve user's data");
                } else {
                    console.log("Logged in successfully");
                    console.log("END: User Login");
                    res.status(200).send(data);
                }
            });
        }
    });
});
app.post('/image', function(req, res) {
    console.log("BEGIN: Image Uploading");
    var base64ImageData = req.body.data;
    var token = req.body.token;
    var id = req.body.id;
    var folderName = req.body.folderName;
    var imageName = req.body.imageName;
    var imageDescription = req.body.imageDescription;
    var extension = req.body.extension;
    var longitude = req.body.longitude;
    var latitude = req.body.latitude;
    const imagePath = __dirname + "/database/images/user" + id + "/" + folderName + "/" ;
    const imageFile = imageName + ".jpg"
    if(!passwordHash.verify(hashHelper1 + id.toString() + hashHelper2, token)) {
        res.status(401).send("Unauthorized access");
        return;
    }
    var decodedImage = new Buffer(base64ImageData, 'base64');
    gm(decodedImage, 'image.' + extension).quality(40).toBuffer('JPG', function(err, buffer) {
        if (err) {
            console.log("error compressing image: %s", err);
        } else {
            fs.mkdirs(imagePath, function(err) {
                if (err) {
                    console.log("unable to make image path");
                    throw err;
                }
                fs.writeFile(imagePath + imageFile, buffer,function(err){
                    if(err) {
                        console.log("error saving image: %s", err);
                    } else {
                        console.log("image saved successfully");
                        var image =  {
                            imageName: imageName,
                            imageDescription: imageDescription,
                            imageExtension: extension,
                            longitude: longitude,
                            latitude: latitude
                        }
                        fs.readFile(__dirname + "/database/data/" + "user" + id + ".json", 'utf-8', function(err, data) {
                            var userData = null;
                            if (err) {
                                console.log("failed to read user %d database: %s",id ,err);
                                return;
                            }
                            try {
                                userData = JSON.parse(data);
                            } catch (ex) {
                                console.log("failed to parse user %d database: %s",id , ex);
                                return;
                            }
                            var folderAlreadyExists = false;
                            for (var i = 0 ; i < userData.foldersCount ; i++) {
                                if (folderName === userData.folders[i].folderName) {
                                    for (var j = 0 ; j < userData.folders[i].imagesCount ; j++) {
                                        if (imageName === userData.folders[i].images[j].imageName) {
                                            res.status(200).send("An image with the same name already exists in the folder "
                                            + folderName
                                            + ", image updated instead");
                                            console.log("An image with the same name already exists in the folder "
                                            + folderName
                                            + ", image updated instead");
                                            console.log("END: Image Uploading");
                                            userData.folders[i].images[j] = image;
                                            return;
                                        }
                                    }
                                    userData.folders[i].images.push(image);
                                    userData.folders[i].imagesCount++;
                                    folderAlreadyExists = true;
                                    break;
                                }
                            }
                            if (!folderAlreadyExists) {
                                var folder = {
                                    folderName: folderName,
                                    imagesCount: 0,
                                    images: []
                                }
                                folder.images.push(image);
                                folder.imagesCount++;
                                userData.folders.push(folder);
                                userData.foldersCount++;
                            }
                            console.log("image added successfully: %s", JSON.stringify(userData));
                            res.status(201).send("Image uploaded successfully");
                            console.log("END: Image Uploading");
                            fs.writeFile(__dirname + "/database/data/" + "user" + id + ".json", JSON.stringify(userData), function(err) {
                                if (err) {
                                    console.log("failed to write image to user %d database: %s", id, err);
                                }
                            });
                        });
                    }
                });
            })
        }
    });
});
app.get('/image', function(req, res) {
    console.log("BEGIN: Image Downloading");
    var token = req.query.token;
    var id = req.query.id;
    var folderName = req.query.folderName;
    var imageName = req.query.imageName;
    const imagePath = __dirname + "/database/images/user" + id + "/" + folderName + "/" ;
    const imageFile = imageName + ".jpg";
    console.log("image downloading request body: %s", JSON.stringify(req.query));
    if(!passwordHash.verify(hashHelper1 + id.toString() + hashHelper2, token)) {
        res.status(401).send("Unauthorized access");
        console.log("Error: Unauthorized access");
        return;
    }
    console.log("image file path %s", imagePath + imageFile);
    fs.readFile(imagePath + imageFile, 'binary',function(err, data) {
        if (err) {
            console.log("failed to read image %s of user %s", imageName, id.toString());
            res.status(404).send("The specified image was not found");
            return;
        }
        var base64EncodedImage = new Buffer(data, 'binary').toString('base64');
        res.status(200).send(base64EncodedImage);
        console.log("END: Image Downloading");
    });
});


app.get('/user', function(req, res) {//Deprecated
    console.log("BEGIN: Requesting User Data");
    var token = req.query.token;
    var id = req.query.id;

    const userPath = __dirname + "/database/data/user" + id + ".json"
    console.log("fetch user data request body: %s", JSON.stringify(req.query));

    if(!passwordHash.verify(hashHelper1 + id.toString() + hashHelper2, token)) {
        res.status(401).send("Unauthorized access");
        console.log("Error: Unauthorized access");
        return;
    }
    console.log("user data path %s", userPath);
    fs.readFile(userPath, 'utf-8',function(err, data) {
        if (err) {
            console.log("failed to read user data of user %s", id.toString());
            res.status(404).send("user with sent id was not found");
            return;
        }
        res.status(200).send(data);
        console.log("END: Requesting User Data");
    });
});


app.delete('/image', function(req, res) {
    console.log("BEGIN: Image Deletion");
    var id = req.body.id;
    console.log("delete folder request body: " + JSON.stringify(req.body));
    var token = req.body.token;
    var folderName = req.query.folderName;
    var imageName = req.query.imageName;
    if (!passwordHash.verify(hashHelper1 + id.toString() + hashHelper2, token)) {
        res.status(401).send("Unauthorized access");
        console.log("Error: Unauthorized access");
        return;
    }
    if (folderName == null || imageName == null) {
        res.status(400).send("Image path contains unsupported characters");
        console.log("Error: Image path contains unsupported characters");
        return;
    }
    const folderPath = __dirname + "/database/images/user" + id + '/' + folderName;
    const imagePath = folderPath + '/' + imageName + ".jpg";
    fs.stat(imagePath, function(err, stats) {
        if (err) {
            console.log("file not found: %s", err);
        }
        fs.remove(imagePath, function(err) {
            if (err) {
                res.status(500).send("error deleting file");
                console.log("error deleting file: %s", err);
            }
            var folderFound = false;
            var imageFound = false;
            fs.readFile(__dirname + "/database/data/" + "user" + id + ".json", 'utf-8', function(err, data) {
                var userData = JSON.parse(data);
                for (var i = 0 ; i < userData.foldersCount && !folderFound ; i++) {
                    if (folderName === userData.folders[i].folderName) {
                        folderFound = true;
                        console.log(JSON.stringify(userData.folders[i].imagesCount));
                        var imagesCount = userData.folders[i].imagesCount;
                        for (var j = 0 ; j < imagesCount && !imageFound ; j++) {
                            if (imageName === userData.folders[i].images[j].imageName) {
                                imageFound = true;
                                userData.folders[i].images.splice(j, 1);
                                userData.folders[i].imagesCount--;
                                if (userData.folders[i].imagesCount === 0) {
                                    userData.folders.splice(i, 1);
                                    userData.foldersCount--;
                                    fs.remove(folderPath, function(err) {
                                        if (err) {
                                            console.log("error couldn't delete empty folder %s", folderName);
                                        }
                                    });
                                }
                            }
                        }
                    }

                }
                fs.writeFile(__dirname + "/database/data/" + "user" + id + ".json", JSON.stringify(userData), function(err) {
                   if (err) {
                       console.log("failed to write changes to user database %s", err);
                   }
                });
                if (imageFound) {
                    console.log("Image Deleted Successfully: user data after image deletion %s", JSON.stringify(userData));
                    res.status(200).send("Image deleted successfully");
                } else {
                    console.log("Failed to delete image");
                    res.status(404).send("image not found");
                }
                console.log("END: Image Deletion");
            });
        });
    });
});
app.delete('/folder', function(req, res) {
    console.log("BEGIN: Folder Deletion");
    var id = req.body.id;
    var token = req.body.token;
    var folderName = req.query.folderName;
    if (!passwordHash.verify(hashHelper1 + id.toString() + hashHelper2, token)) {
        res.status(401).send("Unauthorized access");
        console.log("Error: Unauthorized access");
        return;
    }
    if (folderName == null) {
        res.status(400).send("Folder path contains unsupported characters");
        console.log("Error: Folder path contains unsupported characters");
        return;
    }
    const folderPath = __dirname + "/database/images/user" + id + '/' + folderName;
    fs.stat(folderPath, function(err, stats) {
        console.log("folder stats: %s", stats);
        if (err) {
            console.log("folder not found: %s", err);
        }
        fs.remove(folderPath, function(err) {
            if (err) {
               console.log("error deleting folder: %s", err);
            }
            fs.readFile(__dirname + "/database/data/" + "user" + id + ".json", 'utf-8', function(err, data) {
                var userData = JSON.parse(data);
                var folderFound = false;
                for (var i = 0 ; i < userData.foldersCount && !folderFound ; i++) {
                    if (folderName === userData.folders[i].folderName) {
                        folderFound = true;
                        userData.folders.splice(i, 1);
                        userData.foldersCount--;
                    }
                }
                fs.writeFile(__dirname + "/database/data/" + "user" + id + ".json", JSON.stringify(userData), function(err) {
                    if (err) {
                        console.log("failed to write changes to user database %s", err);
                    }
                });
                if (folderFound) {
                    console.log("Folder deleted successfully: user data after folder deletion %s", JSON.stringify(userData));
                    res.status(200).send("folder deleted successfully");
                } else {
                    console.log("Failed to delete folder");
                    res.status(404).send("folder not found");
                }
                console.log("END: Folder Deletion");
            });
        });
    });
});


var server = app.listen(port, function() {
   console.log("Server Listening at http://127.0.0.1:" + port)
});
